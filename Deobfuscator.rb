#!/usr/bin/env ruby
# Encoding: utf-8
# frozen_string_literal: true

# Main class for the deobfuscator
class Deobfuscator
  require_relative '../parser/Parser.rb'

  def initialize(debug = false, ret_text = false)
    puts 'New deobfuscator initialized' if debug
    @debug    = debug
    @ret_text = ret_text
  end

  def start(filename)
    p  = Parser.new @debug
    pt = p.start filename

    if @ret_text
      # Fire up the Printer class
      # and make our output text
    end
    pt
  end
end
